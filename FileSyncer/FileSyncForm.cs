﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Linq;
using System.Threading;

// Custom Library
using Microsoft.WindowsAPICodePack.Dialogs;
using LibraryCode;

namespace FileSyncer {

    /// <summary>
    /// Main entry point for Form
    /// </summary>
    public partial class FileSyncForm : Form {

        #region Variables

        private CommonOpenFileDialog OpenFolder;
        private string RootDirectory, User;
        private ToolTip Tips, CreditTip;

        private List<SyncingData> SyncData;
        private List<Thread> SyncThread;

        #endregion Variables

        /// <summary>
        /// Class containing all the sync data
        /// </summary>
        private class SyncingData {
            public string DCode { get; set; }

            public string Descript { get; set; }

            public string Parent { get; set; }

            public List<string> ToSync { get; set; }
        }

        /// <summary>
        /// Main Startup for FileSyncer
        /// </summary>
        public FileSyncForm ( ) {
            InitializeComponent ( );

            // New Tool tip
            Tips = new ToolTip ( );
            Tips.AutoPopDelay = 5000;
            Tips.InitialDelay = 1000;
            Tips.ReshowDelay = 100;
            Tips.UseAnimation = true;
            Tips.UseFading = true;
            Tips.ShowAlways = true;
            Tips.IsBalloon = true;

            CreditTip = new ToolTip ( );
            CreditTip.AutoPopDelay = 7500;
            CreditTip.InitialDelay = 1500;
            CreditTip.ReshowDelay = 250;
            CreditTip.UseAnimation = true;
            CreditTip.UseFading = true;

            // Set the Tool tips text
            // Title Tips
            Tips.SetToolTip ( LTitle, "Description this sync so you can reference it later." );
            Tips.SetToolTip ( TTitle, "Description this sync so you can reference it later." );
            // Description Tips
            Tips.SetToolTip ( LDescript, "Description this sync so you can reference it later." );
            Tips.SetToolTip ( TDescript, "Description this sync so you can reference it later." );
            // Parent Tips
            Tips.SetToolTip ( BBrowseParent, "Click here to browser for a parent folder." );
            Tips.SetToolTip ( LParent, "The location of the folder that all the other folders will be synced with." );
            Tips.SetToolTip ( TParent, "The location of the folder that all the other folders will be synced with." );
            // ToSyncFolder Tips
            Tips.SetToolTip ( BAddFolder, "Click here to browser for a folder to sync to." );
            Tips.SetToolTip ( BRemove, "Click here remove a folder from the sync." );
            Tips.SetToolTip ( LToSync, "The location of the folder(s) that will be synced with the parent folder." );
            Tips.SetToolTip ( ToSyncFolders, "The location of the folder(s) that will be synced with the parent folder." );
            // Save TIps
            Tips.SetToolTip ( BSaveSync, "Save the current configuration of this sync to a file." );
            // Start Tips
            Tips.SetToolTip ( BStartSync, "Start this sync. Configuration WILL NOT BE SAVED, use that save button to save your configuration." );
            // DropBox TIps
            Tips.SetToolTip ( LSaved, "List of all saved sync under your name. Choose a file to load its configuration." );
            Tips.SetToolTip ( CBSaved, "List of all saved sync under your name. Choose a file to load its configuration." );
            Tips.SetToolTip ( BRemoveSaved, "Remove the currently selected configuration file from List AND computer. CANNOT BE UNDO!" );
            Tips.SetToolTip ( LCurrent, "List of all started sync. Choose a sync to load its configuration." );
            Tips.SetToolTip ( CBCurrent, "List of all started sync. Choose a sync to load its configuration." );
            Tips.SetToolTip ( BStopSync, "Stop the currently selected sync." );
            // Credit Tips
            CreditTip.SetToolTip ( Credit, "This program is provided as it is. This is still in beta phrase. Most glitches had been fixed though DO NOT EXPECT this program to be a fully functional File Syncer; there are better ways to sync files.\nGlitches will be fixed over time when I have time and can find them. If you have any questions or comments fill free to post a message to me on Edmodo or email me at Comailge@gmail.com." );
            CreditTip.SetToolTip ( Version, "This is still in beta phase; however, I have debugged this program to my best extent so far. There still may be bugs.\nEveryone hates bugs so tell me what those bugs are and I will fix them as soon as possible. Read the tool tip over to the left for contact information." );

            // Set a new instance of Common  File Dialog
            OpenFolder = new CommonOpenFileDialog ( );
            // Set basic properties
            OpenFolder.IsFolderPicker = true;
            OpenFolder.AddToMostRecentlyUsedList = true;
            OpenFolder.AllowPropertyEditing = false;
            OpenFolder.EnsureFileExists = true;
            OpenFolder.EnsurePathExists = true;

            // Get the root directory of the program
            RootDirectory = Environment.CurrentDirectory;
            // Get the user ID
            User = Environment.UserName;

            // Load all Configuration Files
            if ( Directory.Exists ( RootDirectory + @"\" + User ) )	// If Directory exists
				foreach ( string name in Directory.GetFiles( RootDirectory + @"\" + User ) )
                    CBSaved.Items.Add ( Path.GetFileNameWithoutExtension ( name ) );
            else // Else make it
				Directory.CreateDirectory ( RootDirectory + @"\" + User );

            // Initialize new Lists
            SyncData = new List<SyncingData> ( );
            SyncThread = new List<Thread> ( );

            // Center the Window
            CenterToScreen ( );
        }

        #region EventHandler

        /// <summary>
        /// Choose a Parent Folder
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BrowserParentClick ( object sender, EventArgs e ) {
            OpenFolder.Title = "Choose a Parent Folder...";
            OpenFolder.Multiselect = false;

            if ( OpenFolder.ShowDialog ( ) == CommonFileDialogResult.Ok ) {
                if ( ToSyncFolders.Items.Contains ( OpenFolder.FileName ) ) {
                    if ( MessageBox.Show ( "This Folder: " + OpenFolder.FileName + ", is listed under Folders to Sync! You cannot Sync the Parent Folder with the Parent Folder! Do you want me to label this folder as a Parent Folder instead of a Child?", "Parent Folder Already in List", MessageBoxButtons.YesNo, MessageBoxIcon.Error ) == DialogResult.No )
                        return;
                    else
                        ToSyncFolders.Items.Remove ( OpenFolder.FileName );
                }
                TParent.Text = OpenFolder.FileName;
            }
            CBSaved.SelectedIndex = -1;
        }

        /// <summary>
        /// Add a Folder to Sync to
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddFolders ( object sender, EventArgs e ) {
            OpenFolder.Title = "Choose the Child Folder(s)...";
            OpenFolder.Multiselect = true;

            if ( OpenFolder.ShowDialog ( ) == CommonFileDialogResult.Ok ) {
                foreach ( string location in OpenFolder.FileNames ) { // Add each location to ToSyncFolder
                    if ( !ToSyncFolders.Items.Contains ( location ) ) {	// If the location is not already in the List
                        if ( TParent.Text.Equals ( location ) ) {	// If the determine whether the location is Parent
                            if ( MessageBox.Show ( "This Folder, " + location + ", is the Parent Folder! You cannot Sync the Parent Folder with the Parent Folder!. Do you want me to add this folder to the list?", "Cannot Add Parent to Child Folder(s) List", MessageBoxButtons.YesNo, MessageBoxIcon.Error ) == DialogResult.Yes ) {	// If it is then do the User want it as the Parent or Child?
                                ToSyncFolders.Items.Add ( location );
                                TParent.Text = "Parent Location - Please use the Browser Button to Locate a Folder"; // Reset the Parent Text
                            }
                        } else
                            ToSyncFolders.Items.Add ( location );
                    }
                }
            }
            CBSaved.SelectedIndex = -1;
        }

        /// <summary>
        /// Remove a Folder from sync
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RemoveFolders ( object sender, EventArgs e ) {
            if ( ToSyncFolders.SelectedItems.Count > 0 ) {
                for ( int x = ToSyncFolders.SelectedIndices.Count - 1; x >= 0; x-- ) {
                    ToSyncFolders.Items.RemoveAt ( ToSyncFolders.SelectedIndices [ x ] );
                }
            }
            CBSaved.SelectedIndex = -1;
        }

        /// <summary>
        /// Save Sync to a Configuration File
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SaveSync ( object sender, EventArgs e ) {
            // Check for all requirements
            // Must have a title, a parent folder and a child folder 
            if ( !string.IsNullOrEmpty ( TTitle.Text ) && TParent.Text.Contains ( ":" ) && ToSyncFolders.Items.Count != 0 ) {

                // Either New Configuration or Override the Configuration
                if ( Exist ( "Save" ) ) {	// If the title of sync do not exist
                    if ( string.IsNullOrEmpty ( TID.Text ) )	 // Check to see if an ID have been created for this sync
						TID.Text = new Random ( ).RandomAlphaNumeric ( 75 );	 // If not then create one

                    List<string> Data = new List<string> ( ), // List of all Data
                    ToSyncTemp = new List<string> ( );	// A temporary List of a child folders (to add all the objects from the ListBox Collection)
                    for ( int x = ToSyncFolders.Items.Count - 1; x >= 0; x-- )
                        ToSyncTemp.Add ( ToSyncFolders.Items [ x ].ToString ( ) );	// Get all the child folder(s)

                    // Add Data to a List to write to file
                    Data.Add ( TID.Text );
                    Data.Add ( TTitle.Text );
                    Data.Add ( TDescript.Text );
                    Data.Add ( TParent.Text );
                    Data.AddRange ( ToSyncTemp );

                    // Write to File
                    File.WriteAllLines ( RootDirectory + @"\" + User + @"\" + TTitle.Text + ".config", Data );

                    // Add NEW Configuration to ComboBox
                    CBSaved.Items.Add ( TTitle.Text );
                }
            } else	 // Missing one or more requirements
				MessageBox.Show ( "One or more required fields is incomplete! Please fix this before starting sync", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error );
        }

        /// <summary>
        /// Remove a configuration
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RemoveSaved ( object sender, EventArgs e ) {
            // Check to see if configuration exist
            if ( File.Exists ( RootDirectory + @"\" + User + @"\" + CBSaved.Text + ".config" ) )
                File.Delete ( RootDirectory + @"\" + User + @"\" + CBSaved.Text + ".config" );	// If it does then DELETE!
			else // If for some reason it was already deleted but still in List... warn the user
				MessageBox.Show ( "This configuration file was already deleted...", "Error Deleting Configuration File", MessageBoxButtons.OK, MessageBoxIcon.Error );

            // Remove from List
            CBSaved.Items.Remove ( CBSaved.SelectedItem );
        }

        /// <summary>
        /// Start Sync
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StartSync ( object sender, EventArgs e ) {

            // Check for all requirements
            // Must have a title, a parent folder and a child folder 
            if ( !string.IsNullOrEmpty ( TTitle.Text ) && TParent.Text.Contains ( ":" ) && ToSyncFolders.Items.Count != 0 ) {

                if ( Exist ( "Start" ) ) {   // If the title of sync do not exist
                    if ( string.IsNullOrEmpty ( TID.Text ) )	 // Check to see if an ID have been created for this sync
						TID.Text = new Random ( ).RandomAlphaNumeric ( 75 );	 // If not then create one

                    List<string> ToSyncTemp = new List<string> ( );	 // A temporary List of a child folders (to add all the objects from the ListBox Collection)
                    for ( int x = ToSyncFolders.Items.Count - 1; x >= 0; x-- )
                        ToSyncTemp.Add ( ToSyncFolders.Items [ x ].ToString ( ) );	// Get all the child folder(s)

                    // This method uses the fact that the most recent item added is the newest item thus the last item is the one needed to be started.
                    // Add a new instance of the sync data
                    SyncData.Add ( new SyncingData ( )
                        {
                            DCode = TID.Text,
                            Descript = TDescript.Text,
                            Parent = TParent.Text,
                            ToSync = ToSyncTemp
                        } );
                    SyncThread.Add ( new Thread ( StartSync ) );	 // Create a new thread
                    SyncThread [ SyncThread.Count - 1 ].Name = TTitle.Text;	 // Set the name
                    SyncThread [ SyncThread.Count - 1 ].Start ( SyncThread.Count - 1 );	// Start the sync

                    // Add item to ComboBox
                    CBCurrent.Items.Add ( TTitle.Text );
                }
            } else	 // Missing one or more requirements
				MessageBox.Show ( "One or more required fields is incomplete! Please fix this before starting sync", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error );
        }

        /// <summary>
        /// Stop the selected thread from running
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StopSync ( object sender, EventArgs e ) {
            // Get the index of the currently selected thread
            int ThreadIndex = SyncThread.FindIndex ( x => x.Name.Equals ( CBCurrent.Text ) );
            if ( ThreadIndex < 0 )
                return;

            // Stop Thread
            SyncThread [ ThreadIndex ].Abort ( );
            // Wait until the thread stop
            SyncThread [ ThreadIndex ].Join ( );

            // Remove from Currently Running Thread ComboBox
            CBCurrent.Items.Remove ( CBCurrent.SelectedItem );
            SyncThread.RemoveAt ( ThreadIndex );
            SyncData.RemoveAt ( ThreadIndex );
        }

        /// <summary>
        /// Update the Form based on the configuration files that the user made
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SaveUpdateFields ( object sender, EventArgs e ) {
            if ( File.Exists ( RootDirectory + @"\" + User + @"\" + CBSaved.Text + ".config" ) ) {
                List<string> Data = File.ReadAllLines ( RootDirectory + @"\" + User + @"\" + CBSaved.Text + ".config" ).ToList ( );

                // Set the Text Values
                TID.Text = Data [ 0 ];
                TTitle.Text = Data [ 1 ];
                TDescript.Text = Data [ 2 ];
                if ( Directory.Exists ( Data [ 3 ] ) )
                    TParent.Text = Data [ 3 ];

                // Clear and reset the ToSyncFolders Values
                ToSyncFolders.Items.Clear ( );
                for ( int x = Data.Count - 1; x > 3; x-- ) {
                    if ( Directory.Exists ( Data [ x ] ) )
                        ToSyncFolders.Items.Add ( Data [ x ] );
                }
            } else {
                if ( !string.IsNullOrEmpty ( CBSaved.SelectedText ) ) {
                    MessageBox.Show ( "This configuration file does not exist anymore...", "Error Loading Configuration Files", MessageBoxButtons.OK, MessageBoxIcon.Error );
                    CBSaved.Items.Remove ( CBSaved.SelectedItem );
                }
            }
        }

        /// <summary>
        /// Update the Form Based on the configuration of the Thread that the user selected
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SyncUpdateFields ( object sender, EventArgs e ) {
            // Get the index of the currently selected thread
            int ThreadIndex = SyncThread.FindIndex ( x => x.Name.Equals ( CBCurrent.Text ) );

            // Set the Text Values
            TID.Text = SyncData [ ThreadIndex ].DCode;
            TTitle.Text = CBCurrent.Text;
            TDescript.Text = SyncData [ ThreadIndex ].Descript;
            TParent.Text = SyncData [ ThreadIndex ].Parent;

            // Clear and reset the ToSyncFolders Values
            ToSyncFolders.Items.Clear ( );
            for ( int x = SyncData [ ThreadIndex ].ToSync.Count - 1; x >= 0; x-- )
                ToSyncFolders.Items.Add ( SyncData [ ThreadIndex ].ToSync [ x ] );
        }

        /// <summary>
        /// Just updating the text count of the Title TextBox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TitleTextCount ( object sender, EventArgs e ) {
            if ( TTitle.Text.Length < 49 )
                LTitleRemain.Text = ( 50 - TTitle.Text.Length ) + " Characters Remaining";
            else
                LTitleRemain.Text = ( 50 - TTitle.Text.Length ) + " Character Remaining";
        }

        /// <summary>
        /// Title Text Keyboard Shortcuts
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TitleShortcuts ( object sender, KeyEventArgs e ) {
            if ( e.KeyData == ( Keys.Control | Keys.A ) )
                TTitle.SelectAll ( ); // Select all
			else if ( e.KeyData == ( Keys.Control | Keys.Back ) ) {
                e.SuppressKeyPress = true;
                SendKeys.Send ( "^+{LEFT}{BACKSPACE}" );	// Delete group of letters
            }
        }

        /// <summary>
        /// Just updating the text count of the Descriptiong TextBox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DescriptionTextCount ( object sender, EventArgs e ) {
            if ( TDescript.Text.Length < 124 )
                LDescriptRemain.Text = ( 125 - TDescript.Text.Length ) + " Characters Remaining";
            else
                LDescriptRemain.Text = ( 125 - TDescript.Text.Length ) + " Character Remaining";
            CBSaved.SelectedIndex = -1;
        }

        /// <summary>
        /// Description Text Keyboad Shortcuts
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DescriptionShortcuts ( object sender, KeyEventArgs e ) {
            if ( e.KeyData == ( Keys.Control | Keys.A ) )
                TDescript.SelectAll ( );	// Select all
			else if ( e.KeyData == ( Keys.Control | Keys.Back ) ) {
                e.SuppressKeyPress = true;
                SendKeys.Send ( "^+{LEFT}{BACKSPACE}" );	// Delete group of letters
            }
        }

        #endregion Event Handler

        #region Methods

        /// <summary>
        /// Start the sync
        /// </summary>
        /// <param name="pos">Position of the Data Item</param>
        private void StartSync ( object pos ) {
            string parent = SyncData [ ( int )pos ].Parent;
            List<string> Folders = SyncData [ ( int )pos ].ToSync;

            while ( true ) {
                for ( int x = Folders.Count - 1; x >= 0; x-- )
                    Library.CopyDirectoryWithChild ( parent, Folders [ x ] );
            }
        }

        /// <summary>
        /// Check to see whether the currently entered title of sync exists. If so merge data or override?
        /// </summary>
        /// <param name="type">What Button was pressed? Save => Save Sync || Sync => Start Sync</param>
        /// <returns>True => Needs to be Created | False => Same Title</returns>
        private bool Exist ( string type ) {
            // Does this exist?
            if ( !CBSaved.Items.Contains ( TTitle.Text ) && !CBCurrent.Items.Contains ( TTitle.Text ) ) {
                return true;
            }

            #region Initialize
            // Get the ToSyncFolders
            List<string> ToSyncTemp = new List<string> ( );
            for ( int x = ToSyncFolders.Items.Count - 1; x >= 0; x-- )
                ToSyncTemp.Add ( ToSyncFolders.Items [ x ].ToString ( ) );

            // Making SyncingData for New
            MergeForm.SyncingData NewData = new MergeForm.SyncingData ( )
            {
                Parent = TParent.Text,
                Descript = TDescript.Text,
                ToSync = ToSyncTemp
            };
            // Making  SyncingData for Old			
            MergeForm.SyncingData OldData = new MergeForm.SyncingData ( );

            MergeForm MForm = new MergeForm ( );
            #endregion Initialize

            if ( type.Equals ( "Save" ) ) {	// For the Save Button
                #region Save Sync Button
                // Get the Configuration Data
                List<string> ConfigSync = new List<string> ( );
                List<string> Data = File.ReadAllLines ( RootDirectory + @"\" + User + @"\" + TTitle.Text + ".config" ).ToList ( );
                for ( int x = Data.Count - 1; x > 3; x-- )
                    ConfigSync.Add ( Data [ x ] );

                // Is Data (Form vs Configuration) the Same? If so return
                if ( Data [ 3 ] == TParent.Text && Data [ 2 ] == TDescript.Text && ConfigSync.EqualList ( ToSyncTemp ) )
                    return false;

                // Set the SyncingData for the Configuration
                OldData = new MergeForm.SyncingData ( )
                {
                    Parent = Data [ 3 ],
                    Descript = Data [ 2 ],
                    ToSync = ConfigSync
                };

                // Show the MergeBox
                MForm = new MergeForm ( OldData, NewData );
                DialogResult Result = MForm.ShowDialog ( );

                // User has Merged
                if ( Result == DialogResult.OK ) {
                    // Clear out the Data List to reuse it
                    Data.Clear ( );

                    // Set the Data
                    Data.Add ( TID.Text );
                    Data.Add ( TTitle.Text );
                    Data.Add ( MForm.Modified.Descript );
                    Data.Add ( MForm.Modified.Parent );
                    Data.AddRange ( MForm.Modified.ToSync );

                    // Write to File
                    File.WriteAllLines ( RootDirectory + @"\" + User + @"\" + TTitle.Text + ".config", Data );
                }
                #endregion Save Sync Button
            } else if ( type.Equals ( "Start" ) ) {	// For the Start Button
                #region Start Sync Button
                // Get the Index of the Current Thread
                int ThreadIndex = SyncThread.FindIndex ( x => x.Name.Equals ( TTitle.Text ) );

                // Does this exist?
                if ( ThreadIndex < 0 )
                    return true;

                // Is Data (Form vs Configuration) the Same? If so return false
                if ( SyncData [ ThreadIndex ].Parent == TParent.Text && SyncData [ ThreadIndex ].Descript == TDescript.Text && SyncData [ ThreadIndex ].ToSync.EqualList ( ToSyncTemp ) )
                    return false;

                // Set the SyncingData based on the Thread Data
                OldData = new MergeForm.SyncingData ( )
                {
                    Parent = SyncData [ ThreadIndex ].Parent,
                    Descript = SyncData [ ThreadIndex ].Descript,
                    ToSync = SyncData [ ThreadIndex ].ToSync
                };

                // Show the MergeBox
                MForm = new MergeForm ( OldData, NewData );
                DialogResult Result = MForm.ShowDialog ( );

                // User has Merged!
                if ( Result == DialogResult.OK ) {
                    // Update the Thread Data
                    SyncData [ ThreadIndex ] = new SyncingData ( )
                    {
                        Parent = MForm.Modified.Parent,
                        Descript = MForm.Modified.Descript,
                        ToSync = MForm.Modified.ToSync,
                        DCode = TID.Text
                    };

                    // Restart the Thread
                    SyncThread [ ThreadIndex ].Abort ( );
                    SyncThread [ ThreadIndex ].Join ( );
                    SyncThread [ ThreadIndex ] = new Thread ( StartSync );
                    SyncThread [ ThreadIndex ].Name = TTitle.Text;
                    SyncThread [ ThreadIndex ].Start ( ThreadIndex );
                }
                #endregion Start Sync Button
            }

            #region Update Form View
            // Update the Form View
            if ( MForm.Modified != null ) {
                TDescript.Text = MForm.Modified.Descript;
                TParent.Text = MForm.Modified.Parent;
                ToSyncFolders.Items.Clear ( );
                for ( int x = MForm.Modified.ToSync.Count - 1; x >= 0; x-- )
                    ToSyncFolders.Items.Add ( MForm.Modified.ToSync [ x ] );
            }
            #endregion Update Form View

            // Nothing Happen Here!
            return false;
        }

        #endregion Methods
    }
}