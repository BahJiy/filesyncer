﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace LibraryCode {

	/// <summary>
	/// A library of common/generic methods that benefits the programmer
	/// </summary>
	public static class Library {

		#region List

		/// <summary>
		/// Shuffle the List using Fisher-Yates Randomizer
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="list">List to shuffle</param>
		public static void Shuffle<T>( this List<T> list ) {
			Random rand = new Random( );
			int n = list.Count;
			while( n > 1 ) {
				n--;
				int k = rand.Next( n + 1 );
				T value = list[ k ];
				list[ k ] = list[ n ];
				list[ n ] = value;
			}
		}

		/// <summary>
		/// See if a List contains any elements from another List
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="list">Original List</param>
		/// <param name="cList">List to Compare</param>
		/// <returns></returns>
		public static bool ContainList<T>( this List<T> list, List<T> cList ) {
			foreach( T o in cList ) {
				if( list.Contains( o ) )
					return true;
			}

			return false;
		}

		/// <summary>
		/// See if a List contains ALL elements from another List
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="list">Original List</param>
		/// <param name="cList">List to Compare</param>
		/// <returns></returns>
		public static bool ContainListAll<T>( this List<T> list, List<T> cList ) {
			foreach( T o in cList ) {
				if( !list.Contains( o ) )
					return false;
			}

			return true;
		}

		/// <summary>
		/// See if a List contains ALL elements from another List. No More No Less
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="list">Original List</param>
		/// <param name="cList">List to Compare</param>
		/// <returns></returns>
		public static bool EqualList<T>( this List<T> list, List<T> cList ) {
			if( list.Count == cList.Count ) {
				foreach( T o in cList )
					if( !list.Contains( o ) )
						return false;
			} else
				return false;
			return true;
		}

		/// <summary>
		/// Get the sum of all the variable in a list up to a point.A value of Infinity means that there was an error
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="list">List to add</param>
		/// <param name="x">Last index to calculate to</param>
		/// <returns></returns>
		public static double ValueSum<T>( this List<T> list, int x ) {
			double sum = double.PositiveInfinity;
			if( x > list.Count( ) )
				x = list.Count( );
			if( x > 0 ) {
				sum = 0;
				for( int z = 0; z <= x - 1; z++ ) {
					sum += Convert.ToDouble( list[ z ] );
				}
			}
			return sum;
		}

		/// <summary>
		/// Get the sum of all the variable in a list up to a point. A value of Infinity means that there is an error
		/// </summary>
		/// <typeparam name="T">Generic</typeparam>
		/// <param name="list">List to add</param>
		/// <param name="x">Index of first value to add</param>
		/// /// <param name="y">Index of last value to add</param>
		/// <returns>Sum from x to y</returns>
		public static double ValueSum<T>( this List<T> list, int x, int y ) {
			double sum = double.PositiveInfinity;
			if( x > 0 && x <= y && x <= list.Count( ) ) {
				if( y > list.Count( ) )
					y = list.Count( );
				sum = 0;
				for( int z = x - 1; z < y; z++ ) {
					sum += Convert.ToDouble( list[ z ] );
				}
			}
			return sum;
		}

		/// <summary>
		/// The sum of the whole list
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="list">List to add</param>
		/// <returns>Sum of List</returns>
		public static double ValueSum<T>( this List<T> list ) {
			double sum = 0;
			for( int z = 0; z <= list.Count - 1; z++ ) {
				sum += Convert.ToDouble( list[ z ] );
			}
			return sum;
		}

		#endregion List

		#region Random

		/// <summary>
		/// Get a Random Number based on the Fisher-Yates Randomizer
		/// </summary>
		/// <param name="rand"></param>
		/// <param name="end">Randomize to this Number</param>
		/// <returns></returns>
		public static int Shuffle( this Random rand, int end ) {
			List<int> number = new List<int>( );

			// Create a list of numbers from 0 => end
			for( int x = 0; x <= Math.Abs( end ); x++ ) {
				if( end < 0 )  // end is negative then make the number negative
					number.Add( -x );
				else
					number.Add( x );
			}

			// Shuffle the list
			number.Shuffle( );

			return ( number[ rand.Next( number.Count - 1 ) ] );	// Chose a random number from the list
		}

		/// <summary>
		/// Get a Random Number based on the Fisher-Yates Randomizer
		/// </summary>
		/// <param name="rand"></param>
		/// <param name="start">Starting number</param>
		/// <param name="end">Ending number</param>
		/// <returns>Random number</returns>
		public static int Shuffle( this Random rand, int start, int end ) {
			List<int> number = new List<int>( );

			if( start < end )  // If the start > end
				for( int x = start; x <= end; x++ )
					number.Add( x );
			else if( end < start )   // If end > start
				for( int x = end; x <= start; x++ )
					number.Add( x );
			else if( start == end )	// If start = end
				number.Add( start );

			// Shuffle the list
			number.Shuffle( );
			return number[ rand.Next( number.Count - 1 ) ];	// Chose a random number from the list
		}

		/// <summary>
		/// Get a Ranomdly Generated String of Upper Case Letters
		/// </summary>
		/// <param name="rand"></param>
		/// <param name="length">How long the string should be</param>
		/// <returns></returns>
		public static string RandomStringUpper( this Random rand, int length ) {
			string ListofString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

			string result = new string( Enumerable.Repeat( ListofString, length ).Select( s => s[ rand.Shuffle( 25 ) ] ).ToArray( ) );

			return result;
		}

		/// <summary>
		/// Get a Ranomdly Generated String of Lower Case Letters
		/// </summary>
		/// <param name="rand"></param>
		/// <param name="length">How long the string should be</param>
		/// <returns></returns>
		public static string RandomStringLower( this Random rand, int length ) {
			string ListofString = "abcdefghijklmnopqrstuvwxyz";

			string result = new string( Enumerable.Repeat( ListofString, length ).Select( s => s[ rand.Shuffle( 25 ) ] ).ToArray( ) );

			return result;
		}

		/// <summary>
		/// Get a Ranomdly Generated String of Upper and Lower Case Letters
		/// </summary>
		/// <param name="rand"></param>
		/// <param name="length">How long the string should be</param>
		/// <returns></returns>
		public static string RandomString( this Random rand, int length ) {
			string ListofString = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

			string result = new string( Enumerable.Repeat( ListofString, length ).Select( s => s[ rand.Shuffle( 51 ) ] ).ToArray( ) );

			return result;
		}

		/// <summary>
		/// Get a Ranomdly Generated String of Upper Case Letters Numbers
		/// </summary>
		/// <param name="rand"></param>
		/// <param name="length">How long the string should be</param>
		/// <returns></returns>
		public static string RandomAlphaNumericUpper( this Random rand, int length ) {
			string ListofString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

			string result = new string( Enumerable.Repeat( ListofString, length ).Select( s => s[ rand.Shuffle( 35 ) ] ).ToArray( ) );

			return result;
		}

		/// <summary>
		/// Get a Ranomdly Generated String of Low Case Letters with Numbers
		/// </summary>
		/// <param name="rand"></param>
		/// <param name="length">How long the string should be</param>
		/// <returns></returns>
		public static string RandomAlphaNumericLower( this Random rand, int length ) {
			string ListofString = "abcdefghijklmnopqrstuvwxyz0123456789";

			string result = new string( Enumerable.Repeat( ListofString, length ).Select( s => s[ rand.Shuffle( 35 ) ] ).ToArray( ) );

			return result;
		}

		/// <summary>
		/// Get a Ranomdly Generated String of Upper and Lower Case Letters with Numbers
		/// </summary>
		/// <param name="rand"></param>
		/// <param name="length">How long the string should be</param>
		/// <returns></returns>
		public static string RandomAlphaNumeric( this Random rand, int length ) {
			string ListofString = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

			string result = new string( Enumerable.Repeat( ListofString, length ).Select( s => s[ rand.Shuffle( 61 ) ] ).ToArray( ) );
			return result;
		}

		#endregion Random

		#region Directory

		/// <summary>
		/// Remove the Root Reference from the Path
		/// </summary>
		/// <returns>Path - Root</returns>
		/// <param name="Path">Path.</param>
		public static string RemoveRootFromName( string Path ) {
			Path = Path.Remove( 0, 2 );
			return Path;
		}

		/// <summary>
		/// Remove Folder Names from the Path
		/// </summary>
		/// <returns>Path - Number of Folder to Remove.</returns>
		/// <param name="Path">Path.</param>
		/// <param name="Amount">Number of Folder.</param>
		public static string RemovePartFromName( string Path, int Amount ) {
			if( Amount > 0 && Amount <= Path.Count( x => x == '\\' ) ) {
				for( int x = 1; x <= Amount; x++ ) {
					Path = Path.Remove( 0, Path.IndexOf( "\\" ) + 1 );
				}
				return "\\" + Path;
			}
			return Path;
		}

		/// <summary>
		/// Get the name of the directories in a folder. No Path
		/// </summary>
		/// <returns>The directories name.</returns>
		/// <param name="Path">Path to the directory</param>
		public static List<string> GetDirectoriesName( string Path ) {
			List<string> FolderPath = Directory.GetDirectories( Path ).ToList<string>( ),
			FolderName = new List<string>( );

			// Get the name of the directory instead of the path
			for( int x = 0; x <= FolderPath.Count - 1; x++ )
				FolderName.Add( new DirectoryInfo( FolderPath[ x ] ).Name );

			return FolderName;
		}

		/// <summary>
		/// Get the name of the files in a folder. No Path
		/// </summary>
		/// <returns>The files name.</returns>
		/// <param name="path">Path to the directory</param>
		public static List<string> GetFilesName( string path ) {
			List<string> FolderPath = Directory.GetFiles( path ).ToList<string>( ),
			FilesName = new List<string>( );

			// Get the name of the directory instead of the path
			for( int x = 0; x <= FolderPath.Count - 1; x++ )
				FilesName.Add( Path.GetFileName( FolderPath[ x ] ) );

			return FilesName;
		}

		/// <summary>
		/// Get the name of the files in a folder without extension. No Path
		/// </summary>
		/// <returns>The files name.</returns>
		/// <param name="path">Path to the directory</param>
		public static List<string> GetFilesNameWithoutExt( string path ) {
			List<string> FolderPath = Directory.GetFiles( path ).ToList<string>( ),
			FilesName = new List<string>( );

			// Get the name of the directory instead of the path
			for( int x = 0; x <= FolderPath.Count - 1; x++ )
				FilesName.Add( Path.GetFileNameWithoutExtension( FolderPath[ x ] ) );

			return FilesName;
		}

		/// <summary>
		/// Copy files and Create folders based on the Parent folder
		/// </summary>
		/// <param name="Parent">Parent folder path</param>
		/// <param name="Child">Child folder path</param>
		public static void CopyDirectory( string Parent, string Child ) {
			try {
				// Get all the folder names
				List<string> FolderName = GetDirectoriesName( Parent );

				// Create the folder if it does not exist
				for( int y = 0; y <= FolderName.Count - 1; y++ ) {
					if( !Directory.Exists( Child + "\\" + FolderName[ y ] ) )
						Directory.CreateDirectory( Child + "\\" + FolderName[ y ] );
				}

				// Get the name of the file and all the files path from the parent
				List<string> FileName = GetFilesName( Parent );

				// Go through and check all the files
				for( int x = 0; x <= FileName.Count - 1; x++ ) {
					// If file does not exist or if the sizes are different then copy new file from the parent
					if( !File.Exists( Child + "\\" + FileName[ x ] ) || new FileInfo( FileName[ x ] ).Length != new FileInfo( Parent + "\\" + FileName[ x ] ).Length ) {
						if( File.Exists( Child + "\\" + FileName[ x ] ) )
							File.Delete( Child + "\\" + FileName[ x ] );
						File.Copy( Parent + "\\" + FileName[ x ], Child + "\\" + FileName[ x ] );
					}
				}
			} catch ( IOException e ) { Console.BackgroundColor = ConsoleColor.Red; Console.ForegroundColor = ConsoleColor.Green; Console.WriteLine( "File/Folder not found? {0}", e ); }
		}

		/// <summary>
		/// Copy files and folders based on the Parent folder
		/// </summary>
		/// <param name="Parent">Parent folder path</param>
		/// <param name="Child">Child folder path</param>
		public static void CopyDirectoryWithChild( string Parent, string Child ) {
			try {
				// Get all the folder names
				List<string> FolderName = GetDirectoriesName( Parent );

				// Create the folder if it does not exist
				for( int y = 0; y <= FolderName.Count - 1; y++ ) {
					if( !Directory.Exists( Child + "\\" + FolderName[ y ] ) )
						Directory.CreateDirectory( Child + "\\" + FolderName[ y ] );
				}

				// Get the name of the file and all the files path from the parent
				List<string> FileName = GetFilesName( Parent );

				// Go through and check all the files
				for( int x = 0; x <= FileName.Count - 1; x++ ) {

					// If file does not exist or if the sizes are different then copy new file from the parent
					if( !File.Exists( Child + "\\" + FileName[ x ] ) || new FileInfo( Child + "\\" + FileName[ x ] ).Length != new FileInfo( Parent + "\\" + FileName[ x ] ).Length ) {
						if( File.Exists( Child + "\\" + FileName[ x ] ) )
							File.Delete( Child + "\\" + FileName[ x ] );
						File.Copy( Parent + "\\" + FileName[ x ], Child + "\\" + FileName[ x ] );
					}
				}

				// Check to see if the folder inside of the parent have more folders or files
				List<string> NewParent = new List<string>( );

				for( int z = 0; z <= FolderName.Count - 1; z++ ) {
					NewParent = Library.GetDirectoriesName( Parent + "\\" + FolderName[ z ] );   // Folders
					NewParent.AddRange( GetFilesName( Parent + "\\" + FolderName[ z ] ) );   // Files

					// If there are folders/files then create
					if( NewParent.Count > 0 ) {
						CopyDirectoryWithChild( Parent + "\\" + FolderName[ z ], Child + "\\" + FolderName[ z ] );
					}
				}
			} catch ( IOException e ) { Console.BackgroundColor = ConsoleColor.Red; Console.ForegroundColor = ConsoleColor.Green; Console.WriteLine( "File/Folder not found? {0}", e ); }
		}

		#endregion Directory
	}
}