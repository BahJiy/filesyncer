﻿using System;
using System.IO;
using System.Windows.Forms;

namespace FileSyncer {

	/// <summary>
	/// Main entry point
	/// </summary>
	class FileSyncing {

		/// <summary>
		/// Find the current directory and Configuration folder 
		/// </summary>
		[STAThread]
		static void Main( ) {
			try {
				Application.Run( new FileSyncForm( ) );
			} catch(Exception e) {
				ErrorWindow EW = new ErrorWindow( e.ToString( ) );
				Application.Run( EW );
			}
		}
	}
}