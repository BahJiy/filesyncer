﻿namespace FileSyncer {
	/// <summary>
	/// Allow the User to Set the new settings if the Sync Titles are same
	/// </summary>
	partial class MergeForm {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing ) {
			if( disposing && ( components != null ) ) {
				components.Dispose( );
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent( ) {
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.MParent = new System.Windows.Forms.TextBox();
			this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
			this.LNewDescript = new System.Windows.Forms.Label();
			this.LOldDescript = new System.Windows.Forms.Label();
			this.MOldDescript = new System.Windows.Forms.TextBox();
			this.MNewDescript = new System.Windows.Forms.TextBox();
			this.LParent = new System.Windows.Forms.Label();
			this.LDescription = new System.Windows.Forms.Label();
			this.LToSyncFolder = new System.Windows.Forms.Label();
			this.BParent = new System.Windows.Forms.Button();
			this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
			this.BToSync = new System.Windows.Forms.Button();
			this.BCombine = new System.Windows.Forms.Button();
			this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
			this.MCombDescript = new System.Windows.Forms.TextBox();
			this.LCombDescript = new System.Windows.Forms.Label();
			this.Title = new System.Windows.Forms.Label();
			this.BAccept = new System.Windows.Forms.Button();
			this.BCancel = new System.Windows.Forms.Button();
			this.MToSync = new System.Windows.Forms.TextBox();
			this.tableLayoutPanel1.SuspendLayout();
			this.tableLayoutPanel3.SuspendLayout();
			this.tableLayoutPanel2.SuspendLayout();
			this.tableLayoutPanel4.SuspendLayout();
			this.SuspendLayout();
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.tableLayoutPanel1.ColumnCount = 3;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel1.Controls.Add(this.MParent, 1, 0);
			this.tableLayoutPanel1.Controls.Add(this.MToSync, 1, 2);
			this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 1, 1);
			this.tableLayoutPanel1.Controls.Add(this.LParent, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.LDescription, 0, 1);
			this.tableLayoutPanel1.Controls.Add(this.LToSyncFolder, 0, 2);
			this.tableLayoutPanel1.Controls.Add(this.BParent, 2, 0);
			this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 2, 2);
			this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel4, 2, 1);
			this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 67);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 3;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel1.Size = new System.Drawing.Size(831, 273);
			this.tableLayoutPanel1.TabIndex = 0;
			// 
			// MParent
			// 
			this.MParent.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.MParent.Font = new System.Drawing.Font("Times New Roman", 9.75F);
			this.MParent.Location = new System.Drawing.Point(127, 3);
			this.MParent.MaxLength = 7500;
			this.MParent.Name = "MParent";
			this.MParent.ReadOnly = true;
			this.MParent.Size = new System.Drawing.Size(462, 15);
			this.MParent.TabIndex = 12;
			this.MParent.Text = "MParent";
			this.MParent.WordWrap = false;
			// 
			// tableLayoutPanel3
			// 
			this.tableLayoutPanel3.ColumnCount = 2;
			this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel3.Controls.Add(this.LNewDescript, 1, 0);
			this.tableLayoutPanel3.Controls.Add(this.LOldDescript, 0, 0);
			this.tableLayoutPanel3.Controls.Add(this.MOldDescript, 0, 1);
			this.tableLayoutPanel3.Controls.Add(this.MNewDescript, 1, 1);
			this.tableLayoutPanel3.Location = new System.Drawing.Point(127, 35);
			this.tableLayoutPanel3.Name = "tableLayoutPanel3";
			this.tableLayoutPanel3.RowCount = 2;
			this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15.96639F));
			this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 84.03362F));
			this.tableLayoutPanel3.Size = new System.Drawing.Size(462, 152);
			this.tableLayoutPanel3.TabIndex = 2;
			// 
			// LNewDescript
			// 
			this.LNewDescript.AutoSize = true;
			this.LNewDescript.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.LNewDescript.Location = new System.Drawing.Point(234, 0);
			this.LNewDescript.Name = "LNewDescript";
			this.LNewDescript.Size = new System.Drawing.Size(115, 19);
			this.LNewDescript.TabIndex = 11;
			this.LNewDescript.Text = "New Description:";
			// 
			// LOldDescript
			// 
			this.LOldDescript.AutoSize = true;
			this.LOldDescript.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.LOldDescript.Location = new System.Drawing.Point(3, 0);
			this.LOldDescript.Name = "LOldDescript";
			this.LOldDescript.Size = new System.Drawing.Size(108, 19);
			this.LOldDescript.TabIndex = 10;
			this.LOldDescript.Text = "Old Description:";
			// 
			// MOldDescript
			// 
			this.MOldDescript.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.MOldDescript.Font = new System.Drawing.Font("Times New Roman", 9.75F);
			this.MOldDescript.Location = new System.Drawing.Point(3, 27);
			this.MOldDescript.MaxLength = 125;
			this.MOldDescript.Multiline = true;
			this.MOldDescript.Name = "MOldDescript";
			this.MOldDescript.ReadOnly = true;
			this.MOldDescript.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.MOldDescript.Size = new System.Drawing.Size(225, 122);
			this.MOldDescript.TabIndex = 8;
			this.MOldDescript.Text = "MOldDescript";
			// 
			// MNewDescript
			// 
			this.MNewDescript.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.MNewDescript.Font = new System.Drawing.Font("Times New Roman", 9.75F);
			this.MNewDescript.Location = new System.Drawing.Point(234, 27);
			this.MNewDescript.MaxLength = 125;
			this.MNewDescript.Multiline = true;
			this.MNewDescript.Name = "MNewDescript";
			this.MNewDescript.ReadOnly = true;
			this.MNewDescript.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.MNewDescript.Size = new System.Drawing.Size(225, 122);
			this.MNewDescript.TabIndex = 9;
			this.MNewDescript.Text = "MNewDescript";
			// 
			// LParent
			// 
			this.LParent.AutoSize = true;
			this.LParent.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.LParent.Location = new System.Drawing.Point(3, 0);
			this.LParent.Name = "LParent";
			this.LParent.Size = new System.Drawing.Size(108, 19);
			this.LParent.TabIndex = 2;
			this.LParent.Text = "Parent Location:";
			// 
			// LDescription
			// 
			this.LDescription.AutoSize = true;
			this.LDescription.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.LDescription.Location = new System.Drawing.Point(3, 32);
			this.LDescription.Name = "LDescription";
			this.LDescription.Size = new System.Drawing.Size(81, 19);
			this.LDescription.TabIndex = 3;
			this.LDescription.Text = "Description:";
			// 
			// LToSyncFolder
			// 
			this.LToSyncFolder.AutoSize = true;
			this.LToSyncFolder.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.LToSyncFolder.Location = new System.Drawing.Point(3, 190);
			this.LToSyncFolder.Name = "LToSyncFolder";
			this.LToSyncFolder.Size = new System.Drawing.Size(118, 19);
			this.LToSyncFolder.TabIndex = 4;
			this.LToSyncFolder.Text = "Folder(s) to Sync:";
			// 
			// BParent
			// 
			this.BParent.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.BParent.Location = new System.Drawing.Point(595, 3);
			this.BParent.Name = "BParent";
			this.BParent.Size = new System.Drawing.Size(185, 26);
			this.BParent.TabIndex = 8;
			this.BParent.Text = "Switch to New Parent";
			this.BParent.UseVisualStyleBackColor = true;
			this.BParent.Click += new System.EventHandler(this.ParentClick);
			// 
			// tableLayoutPanel2
			// 
			this.tableLayoutPanel2.ColumnCount = 1;
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel2.Controls.Add(this.BToSync, 0, 1);
			this.tableLayoutPanel2.Controls.Add(this.BCombine, 0, 0);
			this.tableLayoutPanel2.Location = new System.Drawing.Point(595, 193);
			this.tableLayoutPanel2.Name = "tableLayoutPanel2";
			this.tableLayoutPanel2.RowCount = 2;
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel2.Size = new System.Drawing.Size(185, 75);
			this.tableLayoutPanel2.TabIndex = 9;
			// 
			// BToSync
			// 
			this.BToSync.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.BToSync.Location = new System.Drawing.Point(3, 43);
			this.BToSync.Name = "BToSync";
			this.BToSync.Size = new System.Drawing.Size(182, 32);
			this.BToSync.TabIndex = 11;
			this.BToSync.Text = "Switch to New Folder(s)";
			this.BToSync.UseVisualStyleBackColor = true;
			this.BToSync.Click += new System.EventHandler(this.SwitchClick);
			// 
			// BCombine
			// 
			this.BCombine.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.BCombine.Location = new System.Drawing.Point(3, 3);
			this.BCombine.Name = "BCombine";
			this.BCombine.Size = new System.Drawing.Size(182, 34);
			this.BCombine.TabIndex = 10;
			this.BCombine.Text = "Combine Folders";
			this.BCombine.UseVisualStyleBackColor = true;
			this.BCombine.Click += new System.EventHandler(this.CombineClick);
			// 
			// tableLayoutPanel4
			// 
			this.tableLayoutPanel4.ColumnCount = 1;
			this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel4.Controls.Add(this.MCombDescript, 0, 1);
			this.tableLayoutPanel4.Controls.Add(this.LCombDescript, 0, 0);
			this.tableLayoutPanel4.Location = new System.Drawing.Point(595, 35);
			this.tableLayoutPanel4.Name = "tableLayoutPanel4";
			this.tableLayoutPanel4.RowCount = 2;
			this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15.45894F));
			this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 84.54106F));
			this.tableLayoutPanel4.Size = new System.Drawing.Size(232, 152);
			this.tableLayoutPanel4.TabIndex = 10;
			// 
			// MCombDescript
			// 
			this.MCombDescript.Font = new System.Drawing.Font("Times New Roman", 9.75F);
			this.MCombDescript.Location = new System.Drawing.Point(3, 26);
			this.MCombDescript.MaxLength = 125;
			this.MCombDescript.Multiline = true;
			this.MCombDescript.Name = "MCombDescript";
			this.MCombDescript.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.MCombDescript.Size = new System.Drawing.Size(225, 123);
			this.MCombDescript.TabIndex = 13;
			this.MCombDescript.Text = "MCombDescript";
			this.MCombDescript.TextChanged += new System.EventHandler(this.UpdateCount);
			// 
			// LCombDescript
			// 
			this.LCombDescript.AutoSize = true;
			this.LCombDescript.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.LCombDescript.Location = new System.Drawing.Point(3, 0);
			this.LCombDescript.Name = "LCombDescript";
			this.LCombDescript.Size = new System.Drawing.Size(209, 19);
			this.LCombDescript.TabIndex = 12;
			this.LCombDescript.Text = "Combine Desciprtion (125 Char)";
			// 
			// Title
			// 
			this.Title.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.Title.AutoSize = true;
			this.Title.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Title.Location = new System.Drawing.Point(105, 9);
			this.Title.Name = "Title";
			this.Title.Size = new System.Drawing.Size(610, 46);
			this.Title.TabIndex = 1;
			this.Title.Text = "You have attempt to Save/Start a Sync with the same title as another one.\r\nPlease" +
    " check the data and change accordingly.";
			this.Title.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// BAccept
			// 
			this.BAccept.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.BAccept.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.BAccept.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.BAccept.Location = new System.Drawing.Point(0, 382);
			this.BAccept.Name = "BAccept";
			this.BAccept.Size = new System.Drawing.Size(858, 37);
			this.BAccept.TabIndex = 2;
			this.BAccept.Text = "Accept Change(s)";
			this.BAccept.UseVisualStyleBackColor = true;
			this.BAccept.Click += new System.EventHandler(this.AcceptChanges);
			// 
			// BCancel
			// 
			this.BCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.BCancel.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.BCancel.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.BCancel.Location = new System.Drawing.Point(0, 345);
			this.BCancel.Name = "BCancel";
			this.BCancel.Size = new System.Drawing.Size(858, 37);
			this.BCancel.TabIndex = 3;
			this.BCancel.Text = "Cancel";
			this.BCancel.UseVisualStyleBackColor = true;
			// 
			// MToSync
			// 
			this.MToSync.AcceptsReturn = true;
			this.MToSync.AcceptsTab = true;
			this.MToSync.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.MToSync.Font = new System.Drawing.Font("Times New Roman", 9.75F);
			this.MToSync.Location = new System.Drawing.Point(127, 193);
			this.MToSync.MaxLength = 75000;
			this.MToSync.Multiline = true;
			this.MToSync.Name = "MToSync";
			this.MToSync.ReadOnly = true;
			this.MToSync.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.MToSync.Size = new System.Drawing.Size(459, 75);
			this.MToSync.TabIndex = 11;
			this.MToSync.Text = "MToSync";
			this.MToSync.WordWrap = false;
			// 
			// MergeForm
			// 
			this.AcceptButton = this.BAccept;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoSize = true;
			this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.CancelButton = this.BCancel;
			this.ClientSize = new System.Drawing.Size(858, 419);
			this.Controls.Add(this.BCancel);
			this.Controls.Add(this.BAccept);
			this.Controls.Add(this.Title);
			this.Controls.Add(this.tableLayoutPanel1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.Name = "MergeForm";
			this.ShowIcon = false;
			this.ShowInTaskbar = false;
			this.Text = "Merge Settings";
			this.tableLayoutPanel1.ResumeLayout(false);
			this.tableLayoutPanel1.PerformLayout();
			this.tableLayoutPanel3.ResumeLayout(false);
			this.tableLayoutPanel3.PerformLayout();
			this.tableLayoutPanel2.ResumeLayout(false);
			this.tableLayoutPanel4.ResumeLayout(false);
			this.tableLayoutPanel4.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.Label LParent;
		private System.Windows.Forms.Label LDescription;
		private System.Windows.Forms.Label LToSyncFolder;
		private System.Windows.Forms.Label Title;
		private System.Windows.Forms.Button BParent;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
		private System.Windows.Forms.Button BToSync;
		private System.Windows.Forms.Button BCombine;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
		private System.Windows.Forms.Label LNewDescript;
		private System.Windows.Forms.Label LOldDescript;
		private System.Windows.Forms.TextBox MOldDescript;
		private System.Windows.Forms.TextBox MNewDescript;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
		private System.Windows.Forms.Label LCombDescript;
		private System.Windows.Forms.TextBox MCombDescript;
		private System.Windows.Forms.Button BAccept;
		private System.Windows.Forms.Button BCancel;
		private System.Windows.Forms.TextBox MParent;
		private System.Windows.Forms.TextBox MToSync;
	}
}