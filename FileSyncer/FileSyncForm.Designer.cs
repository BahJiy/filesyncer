﻿namespace FileSyncer {

	/// <summary>
	/// Holds the containers and components settings
	/// </summary>
	partial class FileSyncForm {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing ) {
			if( disposing && (components != null) ) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FileSyncForm));
			this.TTitle = new System.Windows.Forms.TextBox();
			this.TDescript = new System.Windows.Forms.TextBox();
			this.TParent = new System.Windows.Forms.TextBox();
			this.BBrowseParent = new System.Windows.Forms.Button();
			this.ToSyncFolders = new System.Windows.Forms.ListBox();
			this.BAddFolder = new System.Windows.Forms.Button();
			this.BRemove = new System.Windows.Forms.Button();
			this.BSaveSync = new System.Windows.Forms.Button();
			this.BStartSync = new System.Windows.Forms.Button();
			this.CBSaved = new System.Windows.Forms.ComboBox();
			this.CBCurrent = new System.Windows.Forms.ComboBox();
			this.LTitle = new System.Windows.Forms.Label();
			this.LDescript = new System.Windows.Forms.Label();
			this.LParent = new System.Windows.Forms.Label();
			this.LToSync = new System.Windows.Forms.Label();
			this.LSaved = new System.Windows.Forms.Label();
			this.LCurrent = new System.Windows.Forms.Label();
			this.LID = new System.Windows.Forms.Label();
			this.TID = new System.Windows.Forms.TextBox();
			this.BRemoveSaved = new System.Windows.Forms.Button();
			this.BStopSync = new System.Windows.Forms.Button();
			this.LDescriptRemain = new System.Windows.Forms.Label();
			this.LTitleRemain = new System.Windows.Forms.Label();
			this.Credit = new System.Windows.Forms.Label();
			this.Version = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// TTitle
			// 
			resources.ApplyResources(this.TTitle, "TTitle");
			this.TTitle.Name = "TTitle";
			this.TTitle.TextChanged += new System.EventHandler(this.TitleTextCount);
			this.TTitle.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TitleShortcuts);
			// 
			// TDescript
			// 
			resources.ApplyResources(this.TDescript, "TDescript");
			this.TDescript.Name = "TDescript";
			this.TDescript.TextChanged += new System.EventHandler(this.DescriptionTextCount);
			this.TDescript.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DescriptionShortcuts);
			// 
			// TParent
			// 
			this.TParent.BorderStyle = System.Windows.Forms.BorderStyle.None;
			resources.ApplyResources(this.TParent, "TParent");
			this.TParent.Name = "TParent";
			this.TParent.ReadOnly = true;
			// 
			// BBrowseParent
			// 
			resources.ApplyResources(this.BBrowseParent, "BBrowseParent");
			this.BBrowseParent.Name = "BBrowseParent";
			this.BBrowseParent.UseVisualStyleBackColor = true;
			this.BBrowseParent.Click += new System.EventHandler(this.BrowserParentClick);
			// 
			// ToSyncFolders
			// 
			this.ToSyncFolders.Cursor = System.Windows.Forms.Cursors.Cross;
			resources.ApplyResources(this.ToSyncFolders, "ToSyncFolders");
			this.ToSyncFolders.FormattingEnabled = true;
			this.ToSyncFolders.Name = "ToSyncFolders";
			this.ToSyncFolders.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
			// 
			// BAddFolder
			// 
			resources.ApplyResources(this.BAddFolder, "BAddFolder");
			this.BAddFolder.Name = "BAddFolder";
			this.BAddFolder.UseVisualStyleBackColor = true;
			this.BAddFolder.Click += new System.EventHandler(this.AddFolders);
			// 
			// BRemove
			// 
			resources.ApplyResources(this.BRemove, "BRemove");
			this.BRemove.Name = "BRemove";
			this.BRemove.UseVisualStyleBackColor = true;
			this.BRemove.Click += new System.EventHandler(this.RemoveFolders);
			// 
			// BSaveSync
			// 
			resources.ApplyResources(this.BSaveSync, "BSaveSync");
			this.BSaveSync.Name = "BSaveSync";
			this.BSaveSync.UseVisualStyleBackColor = true;
			this.BSaveSync.Click += new System.EventHandler(this.SaveSync);
			// 
			// BStartSync
			// 
			resources.ApplyResources(this.BStartSync, "BStartSync");
			this.BStartSync.Name = "BStartSync";
			this.BStartSync.UseVisualStyleBackColor = true;
			this.BStartSync.Click += new System.EventHandler(this.StartSync);
			// 
			// CBSaved
			// 
			this.CBSaved.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			resources.ApplyResources(this.CBSaved, "CBSaved");
			this.CBSaved.FormattingEnabled = true;
			this.CBSaved.Name = "CBSaved";
			this.CBSaved.Sorted = true;
			this.CBSaved.SelectedIndexChanged += new System.EventHandler(this.SaveUpdateFields);
			// 
			// CBCurrent
			// 
			this.CBCurrent.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			resources.ApplyResources(this.CBCurrent, "CBCurrent");
			this.CBCurrent.FormattingEnabled = true;
			this.CBCurrent.Name = "CBCurrent";
			this.CBCurrent.Sorted = true;
			this.CBCurrent.SelectedIndexChanged += new System.EventHandler(this.SyncUpdateFields);
			// 
			// LTitle
			// 
			resources.ApplyResources(this.LTitle, "LTitle");
			this.LTitle.Name = "LTitle";
			// 
			// LDescript
			// 
			resources.ApplyResources(this.LDescript, "LDescript");
			this.LDescript.Name = "LDescript";
			// 
			// LParent
			// 
			resources.ApplyResources(this.LParent, "LParent");
			this.LParent.Name = "LParent";
			// 
			// LToSync
			// 
			resources.ApplyResources(this.LToSync, "LToSync");
			this.LToSync.Name = "LToSync";
			// 
			// LSaved
			// 
			resources.ApplyResources(this.LSaved, "LSaved");
			this.LSaved.Name = "LSaved";
			// 
			// LCurrent
			// 
			resources.ApplyResources(this.LCurrent, "LCurrent");
			this.LCurrent.Name = "LCurrent";
			// 
			// LID
			// 
			resources.ApplyResources(this.LID, "LID");
			this.LID.Name = "LID";
			// 
			// TID
			// 
			this.TID.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
			this.TID.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.HistoryList;
			this.TID.BorderStyle = System.Windows.Forms.BorderStyle.None;
			resources.ApplyResources(this.TID, "TID");
			this.TID.Name = "TID";
			this.TID.ReadOnly = true;
			// 
			// BRemoveSaved
			// 
			resources.ApplyResources(this.BRemoveSaved, "BRemoveSaved");
			this.BRemoveSaved.Name = "BRemoveSaved";
			this.BRemoveSaved.UseVisualStyleBackColor = true;
			this.BRemoveSaved.Click += new System.EventHandler(this.RemoveSaved);
			// 
			// BStopSync
			// 
			resources.ApplyResources(this.BStopSync, "BStopSync");
			this.BStopSync.Name = "BStopSync";
			this.BStopSync.UseVisualStyleBackColor = true;
			this.BStopSync.Click += new System.EventHandler(this.StopSync);
			// 
			// LDescriptRemain
			// 
			resources.ApplyResources(this.LDescriptRemain, "LDescriptRemain");
			this.LDescriptRemain.Name = "LDescriptRemain";
			// 
			// LTitleRemain
			// 
			resources.ApplyResources(this.LTitleRemain, "LTitleRemain");
			this.LTitleRemain.Name = "LTitleRemain";
			// 
			// Credit
			// 
			resources.ApplyResources(this.Credit, "Credit");
			this.Credit.Name = "Credit";
			// 
			// Version
			// 
			resources.ApplyResources(this.Version, "Version");
			this.Version.Name = "Version";
			// 
			// FileSyncForm
			// 
			resources.ApplyResources(this, "$this");
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.Version);
			this.Controls.Add(this.Credit);
			this.Controls.Add(this.LTitleRemain);
			this.Controls.Add(this.LDescriptRemain);
			this.Controls.Add(this.BStopSync);
			this.Controls.Add(this.BRemoveSaved);
			this.Controls.Add(this.TID);
			this.Controls.Add(this.LID);
			this.Controls.Add(this.LCurrent);
			this.Controls.Add(this.LSaved);
			this.Controls.Add(this.LToSync);
			this.Controls.Add(this.LParent);
			this.Controls.Add(this.LDescript);
			this.Controls.Add(this.LTitle);
			this.Controls.Add(this.CBCurrent);
			this.Controls.Add(this.CBSaved);
			this.Controls.Add(this.BStartSync);
			this.Controls.Add(this.BSaveSync);
			this.Controls.Add(this.BRemove);
			this.Controls.Add(this.BAddFolder);
			this.Controls.Add(this.ToSyncFolders);
			this.Controls.Add(this.BBrowseParent);
			this.Controls.Add(this.TParent);
			this.Controls.Add(this.TDescript);
			this.Controls.Add(this.TTitle);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.KeyPreview = true;
			this.MaximizeBox = false;
			this.Name = "FileSyncForm";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label LTitle;
		private System.Windows.Forms.TextBox TTitle;

		private System.Windows.Forms.Label LDescript;
		private System.Windows.Forms.TextBox TDescript;

		private System.Windows.Forms.Label LParent;
		private System.Windows.Forms.TextBox TParent;
		private System.Windows.Forms.Button BBrowseParent;

		private System.Windows.Forms.Label LToSync;
		private System.Windows.Forms.ListBox ToSyncFolders;
		private System.Windows.Forms.Button BAddFolder;
		private System.Windows.Forms.Button BRemove;

		private System.Windows.Forms.Button BSaveSync;
		private System.Windows.Forms.Button BStartSync;

		private System.Windows.Forms.Label LSaved;
		private System.Windows.Forms.ComboBox CBSaved;

		private System.Windows.Forms.Label LCurrent;
		private System.Windows.Forms.ComboBox CBCurrent;
		private System.Windows.Forms.Label LID;
		private System.Windows.Forms.TextBox TID;
		private System.Windows.Forms.Button BRemoveSaved;
		private System.Windows.Forms.Button BStopSync;
		private System.Windows.Forms.Label LDescriptRemain;
		private System.Windows.Forms.Label LTitleRemain;
		private System.Windows.Forms.Label Credit;
		private System.Windows.Forms.Label Version;
	}
}