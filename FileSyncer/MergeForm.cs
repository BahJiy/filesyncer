﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

// Custom Library
using LibraryCode;

namespace FileSyncer {

	/// <summary>
	/// Allow the User to Set the new settings if the Sync Titles are same
	/// </summary>
	public partial class MergeForm : Form {

		/// <summary>
		/// The New Data that the User Created
		/// </summary>
		public SyncingData Modified;
		private SyncingData Old, New;


		/// <summary>
		/// Class containing all the sync data
		/// </summary>
		public class SyncingData {
			/// <summary>
			/// Description of the Sync
			/// </summary>
			public string Descript { get; set; }
			/// <summary>
			/// Parent Location
			/// </summary>
			public string Parent { get; set; }
			/// <summary>
			/// Folders to Sync With
			/// </summary>
			public List<string> ToSync { get; set; }
		}

		/// <summary>
		/// Empty Initialize
		/// </summary>
		public MergeForm( ) { }

		/// <summary>
		/// Compare and Allows the User to chose the new settings
		/// </summary>
		/// <param name="Old"></param>
		/// <param name="New"></param>
		public MergeForm( SyncingData Old, SyncingData New ) {
			InitializeComponent( );
			MToSync.Text = "";

			this.Old = Old;
			this.New = New;

			// Check Old Folders
			if( !Directory.Exists( Old.Parent ) )
				Old.Parent = "Does Not Exist: " + Old.Parent;
			for( int x = Old.ToSync.Count - 1; x >= 0; x-- )
				if( !Directory.Exists( Old.ToSync[ x ] ))
					New.ToSync[ x ] = "Does Not Exist: " + Old.ToSync[ x ];

			// Set the Data
			MParent.Text = New.Parent;
			MOldDescript.Text = Old.Descript;
			for( int x = Old.ToSync.Count - 1; x >= 0; x-- ) 
				MToSync.Text += Old.ToSync[ x ] + Environment.NewLine;	 // Add folder to MToSync
			MNewDescript.Text = New.Descript;
			MCombDescript.Text = New.Descript;
			if( MCombDescript.Text.Length > 125 )
				MCombDescript.Text = MCombDescript.Text.Remove( 125 );

			// Check for Same Text and Disable Button(s)
			if( Old.Parent == New.Parent )
				BParent.Enabled = false;
			if( Old.ToSync.EqualList( New.ToSync ) ) {
				BToSync.Enabled = false;
				BCombine.Enabled = false;
			}
		}

		/// <summary>
		/// Switch the Parent Text
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void ParentClick( object sender, EventArgs e ) {
			if( BParent.Text.Contains( "Old" ) ) {
				BParent.Text = "Switch to New Parent";
				MParent.Text = Old.Parent;
			} else {
				BParent.Text = "Switch to Old Parent";
				MParent.Text = New.Parent;
			}
		}

		/// <summary>
		/// Combine the ToSyncFolders
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void CombineClick( object sender, EventArgs e ) {
			MToSync.Text = "";
			for( int x = Old.ToSync.Count - 1; x >= 0; x-- )
				MToSync.Text += Old.ToSync[ x ] + Environment.NewLine;
			for( int x = New.ToSync.Count - 1; x >= 0; x-- ) {
				if( !MToSync.Text.Contains( New.ToSync[ x ] ) )
					MToSync.Text += New.ToSync[ x ] + Environment.NewLine;
			}
		}

		/// <summary>
		/// Accept all Changes and Save
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void AcceptChanges( object sender, EventArgs e ) {
			Modified = new SyncingData( ) { Parent = MParent.Text, Descript = MCombDescript.Text, ToSync = MToSync.Text.Split( new string[ ] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries ).ToList( ) };
		}

		/// <summary>
		/// Switch the ToSyncFolder(s) Set
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void SwitchClick( object sender, EventArgs e ) {
			MToSync.Text = "";
			if( BToSync.Text.Contains( "Old" ) ) {
				BToSync.Text = "Switch to New Folder(s)";
				for( int x = Old.ToSync.Count - 1; x >= 0; x-- )
					MToSync.Text += Old.ToSync[ x ] + Environment.NewLine;
			} else {
				BToSync.Text = "Switch to Old Folder(s)";
				for( int x = New.ToSync.Count - 1; x >= 0; x-- )
					MToSync.Text += New.ToSync[ x ] + Environment.NewLine;
			}
		}

		/// <summary>
		/// Update the Character Count
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void UpdateCount( object sender, EventArgs e ) {
			LCombDescript.Text = "Combine Description (" + ( 125 - MCombDescript.Text.Length ) + " Char)";
		}
	}
}