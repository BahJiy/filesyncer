﻿using System;
using System.Windows.Forms;

namespace FileSyncer {
    /// <summary>
    /// Show the form with the error.
    /// </summary>
    public partial class ErrorWindow : Form {

        /// <summary>
        /// Get the error and show the form
        /// </summary>
        /// <param name="error"></param>
        public ErrorWindow ( string error ) {
            InitializeComponent ( );

            textBox1.Text = error;
        }

        private void button1_Click ( object sender, EventArgs e ) {
            Close ( );
        }
    }
}
